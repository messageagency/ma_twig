<?php

namespace Drupal\ma_twig\TwigExtension;

use Drupal\Core\GeneratedLink;
use Drupal\Core\Render\Markup;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class LinkSpan extends AbstractExtension {

  /**
   * Generates a list of all Twig filters that this extension defines.
   */
  public function getFilters() {
    return [
      new TwigFilter('linkspan', array($this, 'linkSpan')),
    ];
  }

  /**
   * Gets a unique identifier for this Twig extension, corresponding to the
   * service exposed in ma_twig.services.yml.
   */
  public function getName() {
    return 'ma_twig.twig.linkspan';
  }

  /**
   * Adds a span.text within each <a>.
   * @TODO regex is not appropriate here. Use DOMDocument or SimpleXML to
   * manipulate HTML. See Drupal\ma_filters\WrapFilterBase for an example.
   */
  public static function linkSpan($html) {
    if (is_array($html)) {
      if (isset($html['#text'])) {
        $html['#text'] = preg_replace('/<a (.*?)>(.*?)<\/a>/', '<a $1><span class="text">$2</span></a>', $html['#text']);
      }
      if (isset($html['#markup'])) {
        $html['#markup'] = preg_replace('/<a (.*?)>(.*?)<\/a>/', '<a $1><span class="text">$2</span></a>', $html['#markup']);
      }
      // DS title field
      if (isset($html[0]['#markup'])) {
        $html[0]['#markup'] = preg_replace('/<a (.*?)>(.*?)<\/a>/', '<a $1><span class="text">$2</span></a>', $html[0]['#markup']);
      }
    }
    if ($html instanceof Markup) {
      $html = Markup::create(preg_replace('/<a (.*?)>(.*?)<\/a>/', '<a $1><span class="text">$2</span></a>', $html->__toString()));
    }
    if ($html instanceof GeneratedLink) {
      $html->setGeneratedLink(preg_replace('/<a (.*?)>(.*?)<\/a>/', '<a $1><span class="text">$2</span></a>', $html->getGeneratedLink()));
    }
    return $html;
  }
}